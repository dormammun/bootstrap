var gulp      = require('gulp'),
	concat    = require('gulp-concat'),
	less      = require('gulp-less'),
	server    = require('gulp-server-livereload'),
	minifyCSS = require('gulp-minify-css'),
	minifyJS  = require('gulp-uglify'),
	rename    = require('gulp-rename');

gulp.task('css', function() {
	gulp.src('./app/css/less/*.less')
	.pipe(less())
	.pipe(minifyCSS())
	.pipe(rename('main.css'))
	.pipe(gulp.dest('./app/css'));
});

gulp.task('js', function (){
	gulp.src('./app/js/plugins/*.js')
	.pipe(concat('all.js'))
	.pipe(minifyJS())
	.pipe(gulp.dest('./app/js'));
});

gulp.task('webserver', function() {
  gulp.src('./app/')
    .pipe(server({
      livereload: true,
      directoryListing: false,
      defaultFile: 'index.html',
      open: true,
      port: 8080
    }));
});

gulp.task('watch', function (){
	gulp.watch('./app/css/less/*.less', ['css']);
	gulp.watch('./app/js/plugins/*.js', ['js']);
	// gulp.watch('./app/*.html', ['default']);
});

gulp.task('default', ['css', 'js', 'webserver', 'watch']);